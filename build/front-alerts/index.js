const socket = io("http://localhost:23000", {
    path: "/redeem-alerts/"
});
socket.on('Hello_from_server', () => {
    console.log('hello from server');
});
socket.on('reward-redeemed', (reward) => {
    console.log(reward);
    var alert = document.getElementById('alerts');
    alert.innerHTML = `${reward}`;
    setTimeout(() => {
        alert.innerHTML = '';
    }, 10000);
});
//# sourceMappingURL=index.js.map