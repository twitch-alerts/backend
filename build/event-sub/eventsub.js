"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Eventsub = void 0;
const twitch_1 = require("twitch");
const twitch_auth_1 = require("twitch-auth");
const dotenv = require('dotenv');
// const clientId = process.env.CHANNELID;
// const accessToken = process.env.TOKEN; //'9ui1agx6lfg3k14wvec80y5manx006';
const twitch_pubsub_client_1 = require("twitch-pubsub-client");
const eventsub_alerts_1 = require("./eventsub-alerts");
const rewards_1 = require("../enums/rewards");
class Eventsub {
    constructor() {
        this.eventAlerts = new eventsub_alerts_1.EventsubAlerts();
        dotenv.config();
        this.clientId = process.env.CHANNELID;
        this.accessToken = process.env.TOKEN;
        const authProvider = new twitch_auth_1.StaticAuthProvider(this.clientId, this.accessToken);
        this.apiClient = new twitch_1.ApiClient({ authProvider });
        this.pubSubClient = new twitch_pubsub_client_1.PubSubClient();
        this.RedemptionListener().then(r => console.log(r));
    }
    async RedemptionListener() {
        const userId = await this.pubSubClient.registerUserListener(this.apiClient);
        const listener = await this.pubSubClient.onRedemption(userId, (message) => {
            console.log(`${message.userDisplayName} redeemed ${message.rewardName} with ${message.rewardCost}!`);
            if (Object.keys(rewards_1.Rewards).includes(message.rewardName)) {
                this.eventAlerts.sendMessage(rewards_1.Rewards[message.rewardName], 'reward-redeemed');
                this.eventAlerts.sendMessage(`<div class="claim-item"><div>${message.userDisplayName} redeemed ${message.rewardName}</div><div>Valor: ${message.rewardCost}</div></div>`, 'reward-redeemed-data');
            }
        });
    }
}
exports.Eventsub = Eventsub;
//# sourceMappingURL=eventsub.js.map