"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsubAlerts = void 0;
const express_1 = __importDefault(require("express"));
const http_1 = require("http");
const cors = require('cors');
const socket_io_1 = require("socket.io");
const app = express_1.default();
/*
const httpServer = createServer(app);
const io = new Server(httpServer,{
  path: '/redeem-alerts/'
});
io.on('connection', (socket) => {
  console.log('connection');
  socket.emit('Hello from server');
});
httpServer.listen(23000)
*/
class EventsubAlerts {
    constructor() {
        this.app = app;
        this.httpServer = http_1.createServer(app);
        this.io = new socket_io_1.Server(this.httpServer, {
            cors: {
                origin: '*',
                allowedHeaders: ["Access-Control-Allow-Origin"]
            },
            path: '/redeem-alerts/'
        });
        this.app.use(cors());
        this.app.use(express_1.default.static("front-alerts"));
        this.onConnect();
        this.onDisconnect();
        this.httpServer.listen(23000, () => {
            console.log('App running on http://localhost:23000');
        });
        /*function check(req) {
          return true;
        }*/
    }
    onConnect() {
        this.io.on('connection', (socket) => {
            console.log('connection');
            socket.emit('Hello_from_server', 'hello-from-server');
        });
    }
    onDisconnect() {
        this.io.on('error', (error) => {
            console.log(error);
        });
    }
    sendMessage(messsage, messageEmit) {
        console.log('emit message');
        this.io.emit(messageEmit, messsage);
    }
}
exports.EventsubAlerts = EventsubAlerts;
//# sourceMappingURL=eventsub-alerts.js.map