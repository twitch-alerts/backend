import { ApiClient } from 'twitch';
import { StaticAuthProvider } from 'twitch-auth';
const dotenv = require('dotenv');
// const clientId = process.env.CHANNELID;
// const accessToken = process.env.TOKEN; //'9ui1agx6lfg3k14wvec80y5manx006';
import {PubSubClient, PubSubRedemptionMessage} from 'twitch-pubsub-client';
import {EventsubAlerts} from "./eventsub-alerts";
import {Rewards} from "../enums/rewards";

export class Eventsub {
  private apiClient;
  private pubSubClient;
  private clientId;
  private accessToken;
  public eventAlerts = new EventsubAlerts();
  constructor() {
    dotenv.config();
    this.clientId = process.env.CHANNELID;
    this.accessToken = process.env.TOKEN;
    const authProvider = new StaticAuthProvider(this.clientId, this.accessToken);
    this.apiClient = new ApiClient({ authProvider });
    this.pubSubClient = new PubSubClient();
    this.RedemptionListener().then(r => console.log(r));
  }

  public async RedemptionListener(){
    const userId = await this.pubSubClient.registerUserListener(this.apiClient);
    const listener = await this.pubSubClient.onRedemption(userId, (message:PubSubRedemptionMessage) => {
      console.log(`${message.userDisplayName} redeemed ${message.rewardName} with ${message.rewardCost}!`)
      if(Object.keys(Rewards).includes(message.rewardName)){
        this.eventAlerts.sendMessage(Rewards[message.rewardName],'reward-redeemed');
        this.eventAlerts.sendMessage(`<div class="claim-item"><div>${message.userDisplayName} redeemed ${message.rewardName}</div><div>Valor: ${message.rewardCost}</div></div>`, 'reward-redeemed-data')
      }
    });
  }
}
