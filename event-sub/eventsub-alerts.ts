import express from 'express';
import {createServer} from 'http';
const cors = require('cors');
import {Server} from "socket.io";

const app = express();
/*
const httpServer = createServer(app);
const io = new Server(httpServer,{
  path: '/redeem-alerts/'
});
io.on('connection', (socket) => {
  console.log('connection');
  socket.emit('Hello from server');
});
httpServer.listen(23000)
*/

export class EventsubAlerts{
  private app = app;
  private httpServer = createServer(app);
  private io = new Server(this.httpServer,{
    cors:{
      origin: '*',
      allowedHeaders: ["Access-Control-Allow-Origin"]
    },
    path: '/redeem-alerts/'
  });

  constructor() {
    this.app.use(cors())
    this.app.use(express.static("front-alerts"));
    this.onConnect();
    this.onDisconnect();
    this.httpServer.listen(23000, () => {
      console.log('App running on http://localhost:23000');
    })
    /*function check(req) {
      return true;
    }*/
  }

  public onConnect(){
    this.io.on('connection', (socket) => {
      console.log('connection');
      socket.emit('Hello_from_server', 'hello-from-server');
    });
  }

  public onDisconnect(){

    this.io.on('error', (error)=>{
      console.log( error)
    })
  }

  public sendMessage(messsage, messageEmit) {
    console.log('emit message');
    this.io.emit(messageEmit, messsage);
  }
}

